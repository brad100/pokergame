using System;
using System.Collections.Generic;
using System.Linq;

namespace Testing
{
    class starter
    {
        static void Main()
        {
            Game.Deck poker = new Game.Deck();
            poker.newGame(); //Main Game function to shuffle and deal random Cards to both players

            //Testing.starter.tester(); //Main Test function for testing all scenarios as need be


            //Made these test functions for the more rare poker hands
            /*
            Console.WriteLine("Testing Flush");
            Testing.starter.testFlush();
            Console.WriteLine();

            Console.WriteLine("Testing Straight Flush");
            Testing.starter.testStraightFlush();
            Console.WriteLine();

            Console.WriteLine("Testing Full House");
            Testing.starter.testfullHouse();
            Console.WriteLine();

            Console.WriteLine("Testing Four of a Kind");
            Testing.starter.testfourOfAKind();
            Console.WriteLine();

            Console.WriteLine("Testing Straight");
            Testing.starter.testStraight();
            Console.WriteLine();
            */
        }

        public static void tester() //This function is for testing any of the winning hand scenarios
        {
            List<Game.Cards> blackHand = new List<Game.Cards>();
            List<Game.Cards> whiteHand = new List<Game.Cards>();
            Dictionary<string, Game.Cards> winningCard = new Dictionary<string, Game.Cards>();


            Game.Cards blackCard1 = new Game.Cards(5, "5", "H");
            Game.Cards blackCard2 = new Game.Cards(5, "5", "H");
            Game.Cards blackCard3 = new Game.Cards(11, "J", "H");
            Game.Cards blackCard4 = new Game.Cards(11, "J", "H");
            Game.Cards blackCard5 = new Game.Cards(13, "K", "H");

            Game.Cards whiteCard1 = new Game.Cards(4, "4", "C");
            Game.Cards whiteCard2 = new Game.Cards(4, "4", "C");
            Game.Cards whiteCard3 = new Game.Cards(6, "6", "C");
            Game.Cards whiteCard4 = new Game.Cards(6, "6", "C");
            Game.Cards whiteCard5 = new Game.Cards(13, "K", "C");


            blackHand = Game.Cards.organizeHandbyValue(blackHand);
            whiteHand = Game.Cards.organizeHandbyValue(whiteHand);

            blackHand.Add(blackCard1);
            blackHand.Add(blackCard2);
            blackHand.Add(blackCard3);
            blackHand.Add(blackCard4);
            blackHand.Add(blackCard5);


            whiteHand.Add(whiteCard1);
            whiteHand.Add(whiteCard2);
            whiteHand.Add(whiteCard3);
            whiteHand.Add(whiteCard4);
            whiteHand.Add(whiteCard5);

            Console.Write("Black: ");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(blackHand[i].Symbol);
                Console.Write(blackHand[i].Suit);
                Console.Write(" ");
            }
            Console.Write("White: ");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(whiteHand[i].Symbol);
                Console.Write(whiteHand[i].Suit);
                Console.Write(" ");
            }

            winningCard = Scoring.handScoring.twoPairs(blackHand, whiteHand);


            foreach (var item in winningCard)
            {
                Console.WriteLine();
                Console.WriteLine();
                Console.Write(item.Key);
                Console.Write(" won with a ");
                Console.Write(item.Value.Symbol);
                Console.Write(item.Value.Suit);
                Console.Write(" in ");
                Console.Write("twoPairs");
                Console.WriteLine();
            }
        }

        public static void testStraightFlush()
        {
            List<Game.Cards> blackHand = new List<Game.Cards>();
            List<Game.Cards> whiteHand = new List<Game.Cards>();
            Dictionary<string, Game.Cards> winningCard = new Dictionary<string, Game.Cards>();


            Game.Cards blackCard1 = new Game.Cards(9, "9", "H");
            Game.Cards blackCard2 = new Game.Cards(10, "T", "H");
            Game.Cards blackCard3 = new Game.Cards(11, "J", "H");
            Game.Cards blackCard4 = new Game.Cards(12, "Q", "H");
            Game.Cards blackCard5 = new Game.Cards(13, "K", "H");

            Game.Cards whiteCard1 = new Game.Cards(4, "4", "C");
            Game.Cards whiteCard2 = new Game.Cards(4, "4", "C");
            Game.Cards whiteCard3 = new Game.Cards(6, "6", "C");
            Game.Cards whiteCard4 = new Game.Cards(6, "6", "C");
            Game.Cards whiteCard5 = new Game.Cards(13, "K", "C");


            blackHand = Game.Cards.organizeHandbyValue(blackHand);
            whiteHand = Game.Cards.organizeHandbyValue(whiteHand);

            blackHand.Add(blackCard1);
            blackHand.Add(blackCard2);
            blackHand.Add(blackCard3);
            blackHand.Add(blackCard4);
            blackHand.Add(blackCard5);


            whiteHand.Add(whiteCard1);
            whiteHand.Add(whiteCard2);
            whiteHand.Add(whiteCard3);
            whiteHand.Add(whiteCard4);
            whiteHand.Add(whiteCard5);

            Console.Write("Black: ");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(blackHand[i].Symbol);
                Console.Write(blackHand[i].Suit);
                Console.Write(" ");
            }
            Console.Write("White: ");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(whiteHand[i].Symbol);
                Console.Write(whiteHand[i].Suit);
                Console.Write(" ");
            }

            winningCard = Scoring.handScoring.straightFlush(blackHand, whiteHand);


            foreach (var item in winningCard)
            {
                Console.WriteLine();
                Console.WriteLine();
                Console.Write(item.Key);
                Console.Write(" won with a ");
                Console.Write(item.Value.Symbol);
                Console.Write(item.Value.Suit);
                Console.Write(" in ");
                Console.Write("Straight Flush");
                Console.WriteLine();
            }
        }

        public static void testFlush()
        {
            List<Game.Cards> blackHand = new List<Game.Cards>();
            List<Game.Cards> whiteHand = new List<Game.Cards>();
            Dictionary<string, Game.Cards> winningCard = new Dictionary<string, Game.Cards>();


            Game.Cards blackCard1 = new Game.Cards(5, "5", "H");
            Game.Cards blackCard2 = new Game.Cards(5, "5", "C");
            Game.Cards blackCard3 = new Game.Cards(11, "J", "D");
            Game.Cards blackCard4 = new Game.Cards(11, "J", "H");
            Game.Cards blackCard5 = new Game.Cards(13, "K", "H");

            Game.Cards whiteCard1 = new Game.Cards(4, "4", "C");
            Game.Cards whiteCard2 = new Game.Cards(4, "4", "C");
            Game.Cards whiteCard3 = new Game.Cards(6, "6", "C");
            Game.Cards whiteCard4 = new Game.Cards(6, "6", "C");
            Game.Cards whiteCard5 = new Game.Cards(13, "K", "C");


            blackHand = Game.Cards.organizeHandbyValue(blackHand);
            whiteHand = Game.Cards.organizeHandbyValue(whiteHand);

            blackHand.Add(blackCard1);
            blackHand.Add(blackCard2);
            blackHand.Add(blackCard3);
            blackHand.Add(blackCard4);
            blackHand.Add(blackCard5);


            whiteHand.Add(whiteCard1);
            whiteHand.Add(whiteCard2);
            whiteHand.Add(whiteCard3);
            whiteHand.Add(whiteCard4);
            whiteHand.Add(whiteCard5);

            Console.Write("Black: ");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(blackHand[i].Symbol);
                Console.Write(blackHand[i].Suit);
                Console.Write(" ");
            }
            Console.Write("White: ");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(whiteHand[i].Symbol);
                Console.Write(whiteHand[i].Suit);
                Console.Write(" ");
            }

            winningCard = Scoring.handScoring.flush(blackHand, whiteHand);


            foreach (var item in winningCard)
            {
                Console.WriteLine();
                Console.WriteLine();
                Console.Write(item.Key);
                Console.Write(" won with a ");
                Console.Write(item.Value.Symbol);
                Console.Write(item.Value.Suit);
                Console.Write(" in ");
                Console.Write("Flush");
                Console.WriteLine();
            }
        }

        public static void testfullHouse()
        {
            List<Game.Cards> blackHand = new List<Game.Cards>();
            List<Game.Cards> whiteHand = new List<Game.Cards>();
            Dictionary<string, Game.Cards> winningCard = new Dictionary<string, Game.Cards>();


            Game.Cards blackCard1 = new Game.Cards(5, "5", "D");
            Game.Cards blackCard2 = new Game.Cards(5, "5", "C");
            Game.Cards blackCard3 = new Game.Cards(5, "5", "S");
            Game.Cards blackCard4 = new Game.Cards(11, "J", "C");
            Game.Cards blackCard5 = new Game.Cards(11, "J", "H");

            Game.Cards whiteCard1 = new Game.Cards(4, "4", "C");
            Game.Cards whiteCard2 = new Game.Cards(4, "4", "C");
            Game.Cards whiteCard3 = new Game.Cards(6, "6", "C");
            Game.Cards whiteCard4 = new Game.Cards(6, "6", "C");
            Game.Cards whiteCard5 = new Game.Cards(13, "K", "C");


            blackHand = Game.Cards.organizeHandbyValue(blackHand);
            whiteHand = Game.Cards.organizeHandbyValue(whiteHand);

            blackHand.Add(blackCard1);
            blackHand.Add(blackCard2);
            blackHand.Add(blackCard3);
            blackHand.Add(blackCard4);
            blackHand.Add(blackCard5);


            whiteHand.Add(whiteCard1);
            whiteHand.Add(whiteCard2);
            whiteHand.Add(whiteCard3);
            whiteHand.Add(whiteCard4);
            whiteHand.Add(whiteCard5);

            Console.Write("Black: ");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(blackHand[i].Symbol);
                Console.Write(blackHand[i].Suit);
                Console.Write(" ");
            }
            Console.Write("White: ");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(whiteHand[i].Symbol);
                Console.Write(whiteHand[i].Suit);
                Console.Write(" ");
            }

            winningCard = Scoring.handScoring.fullHouse(blackHand, whiteHand);


            foreach (var item in winningCard)
            {
                Console.WriteLine();
                Console.WriteLine();
                Console.Write(item.Key);
                Console.Write(" won with a ");
                Console.Write(item.Value.Symbol);
                Console.Write(item.Value.Suit);
                Console.Write(" in ");
                Console.Write("Full House");
                Console.WriteLine();
            }
        }

        public static void testfourOfAKind()
        {
            List<Game.Cards> blackHand = new List<Game.Cards>();
            List<Game.Cards> whiteHand = new List<Game.Cards>();
            Dictionary<string, Game.Cards> winningCard = new Dictionary<string, Game.Cards>();


            Game.Cards blackCard1 = new Game.Cards(4, "4", "H");
            Game.Cards blackCard2 = new Game.Cards(4, "4", "C");
            Game.Cards blackCard3 = new Game.Cards(4, "4", "D");
            Game.Cards blackCard4 = new Game.Cards(4, "4", "S");
            Game.Cards blackCard5 = new Game.Cards(13, "K", "H");

            Game.Cards whiteCard1 = new Game.Cards(4, "4", "C");
            Game.Cards whiteCard2 = new Game.Cards(4, "4", "C");
            Game.Cards whiteCard3 = new Game.Cards(6, "6", "C");
            Game.Cards whiteCard4 = new Game.Cards(6, "6", "C");
            Game.Cards whiteCard5 = new Game.Cards(13, "K", "C");


            blackHand = Game.Cards.organizeHandbyValue(blackHand);
            whiteHand = Game.Cards.organizeHandbyValue(whiteHand);

            blackHand.Add(blackCard1);
            blackHand.Add(blackCard2);
            blackHand.Add(blackCard3);
            blackHand.Add(blackCard4);
            blackHand.Add(blackCard5);


            whiteHand.Add(whiteCard1);
            whiteHand.Add(whiteCard2);
            whiteHand.Add(whiteCard3);
            whiteHand.Add(whiteCard4);
            whiteHand.Add(whiteCard5);

            Console.Write("Black: ");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(blackHand[i].Symbol);
                Console.Write(blackHand[i].Suit);
                Console.Write(" ");
            }
            Console.Write("White: ");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(whiteHand[i].Symbol);
                Console.Write(whiteHand[i].Suit);
                Console.Write(" ");
            }

            winningCard = Scoring.handScoring.fourOfAKind(blackHand, whiteHand);


            foreach (var item in winningCard)
            {
                Console.WriteLine();
                Console.WriteLine();
                Console.Write(item.Key);
                Console.Write(" won with a ");
                Console.Write(item.Value.Symbol);
                Console.Write(item.Value.Suit);
                Console.Write(" in ");
                Console.Write("Four of a kind");
                Console.WriteLine();
            }
        }

        public static void testStraight()
        {
            List<Game.Cards> blackHand = new List<Game.Cards>();
            List<Game.Cards> whiteHand = new List<Game.Cards>();
            Dictionary<string, Game.Cards> winningCard = new Dictionary<string, Game.Cards>();


            Game.Cards blackCard1 = new Game.Cards(5, "5", "H");
            Game.Cards blackCard2 = new Game.Cards(5, "5", "H");
            Game.Cards blackCard3 = new Game.Cards(11, "J", "H");
            Game.Cards blackCard4 = new Game.Cards(11, "J", "H");
            Game.Cards blackCard5 = new Game.Cards(13, "K", "H");

            Game.Cards whiteCard1 = new Game.Cards(4, "4", "C");
            Game.Cards whiteCard2 = new Game.Cards(5, "5", "C");
            Game.Cards whiteCard3 = new Game.Cards(6, "6", "C");
            Game.Cards whiteCard4 = new Game.Cards(7, "7", "C");
            Game.Cards whiteCard5 = new Game.Cards(8, "8", "C");


            blackHand = Game.Cards.organizeHandbyValue(blackHand);
            whiteHand = Game.Cards.organizeHandbyValue(whiteHand);

            blackHand.Add(blackCard1);
            blackHand.Add(blackCard2);
            blackHand.Add(blackCard3);
            blackHand.Add(blackCard4);
            blackHand.Add(blackCard5);


            whiteHand.Add(whiteCard1);
            whiteHand.Add(whiteCard2);
            whiteHand.Add(whiteCard3);
            whiteHand.Add(whiteCard4);
            whiteHand.Add(whiteCard5);

            Console.Write("Black: ");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(blackHand[i].Symbol);
                Console.Write(blackHand[i].Suit);
                Console.Write(" ");
            }
            Console.Write("White: ");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(whiteHand[i].Symbol);
                Console.Write(whiteHand[i].Suit);
                Console.Write(" ");
            }

            winningCard = Scoring.handScoring.straight(blackHand, whiteHand);


            foreach (var item in winningCard)
            {
                Console.WriteLine();
                Console.WriteLine();
                Console.Write(item.Key);
                Console.Write(" won with a ");
                Console.Write(item.Value.Symbol);
                Console.Write(item.Value.Suit);
                Console.Write(" in ");
                Console.Write("Straight");
                Console.WriteLine();
            }
        }

    }
}