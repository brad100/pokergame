﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Game
{

    public class Deck
    {
        private static Random rng = new Random(); 
        public Deck()
        {
        }
      
        public List<Cards> createDeck()
        {
            List<Cards> gameDeck = new List<Cards>();
            Cards currentCard = new Cards();
            for (int i = 0; i < currentCard.suit.Length; i++) //Creating the deck of cards here. Could use an array with all the cards manually created, but wanted to have a way to store values easily
            {
                for (int j = 0; j < currentCard.symbol.Length; j++)
                {
                    Game.Cards card = new Game.Cards(j + 2, currentCard.symbol[j], currentCard.suit[i]);
                    gameDeck.Add(card);
                }

            }
            return gameDeck;
        }
    
        public Stack<Cards> shuffle(List<Cards> orderedDeck) 
        {
            int n = orderedDeck.Count;
            Stack<Cards> shuffledDeck = new Stack<Cards>();
            while (orderedDeck.Count > 0)
            {
                n = orderedDeck.Count;
                int k = rng.Next(n);
                shuffledDeck.Push(orderedDeck[k]);
                orderedDeck.RemoveAt(k);
            }

            return shuffledDeck;
        }

        public void Distribute(ref List<Cards> blackHand, ref List<Cards> whiteHand, Stack<Cards> shuffledDeck)
        {
            for (int i = 0; i < 5; i++)
            {
                blackHand.Add(shuffledDeck.Pop());
            }

            Console.WriteLine();

            for (int i = 0; i < 5; i++)
            {
                whiteHand.Add(shuffledDeck.Pop());
            }

        }

        public void newGame()
        {
            Cards start = new Cards();
            List<Cards> blackHand = new List<Cards>();
            List<Cards> whiteHand = new List<Cards>();
            start.Distribute(ref blackHand, ref whiteHand, (start.shuffle(start.createDeck())));
            string winningHand = "";
            Dictionary<string, Game.Cards> winningCard = new Dictionary<string, Game.Cards>();


            blackHand = Game.Cards.organizeHandbyValue(blackHand);
            whiteHand = Game.Cards.organizeHandbyValue(whiteHand);


            Console.Write("Black: ");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(blackHand[i].Symbol);
                Console.Write(blackHand[i].Suit);
                Console.Write(" ");            
            }
            Console.Write("White: ");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(whiteHand[i].Symbol);
                Console.Write(whiteHand[i].Suit);
                Console.Write(" ");
            }

            if (winningCard.Count() == 0) 
            {
                winningCard = Scoring.handScoring.straightFlush(blackHand, whiteHand);
                winningHand = "straightFlush";
            }
            if (winningCard.Count() == 0)
            {
                winningCard = Scoring.handScoring.fourOfAKind(blackHand, whiteHand);
                winningHand = "fourOfAKind";
            }
            if (winningCard.Count() == 0)
            {
                winningCard = Scoring.handScoring.fullHouse(blackHand, whiteHand);
                winningHand = "fullHouse";
            }
            if (winningCard.Count() == 0)
            {
                winningCard = Scoring.handScoring.flush(blackHand, whiteHand);
                winningHand = "flush";
            }
            if (winningCard.Count() == 0)
            {
                winningCard = Scoring.handScoring.straight(blackHand, whiteHand);
                winningHand = "straight";
            }
            if (winningCard.Count() == 0)
            {
                winningCard = Scoring.handScoring.threeOfAKind(blackHand, whiteHand);
                winningHand = "threeOfAKind";
            }
            if (winningCard.Count() == 0)
            {
                winningCard = Scoring.handScoring.twoPairs(blackHand, whiteHand);
                winningHand = "twoPairs";
            }
            if (winningCard.Count() == 0)
            {
                winningCard = Scoring.handScoring.pair(blackHand, whiteHand);
                winningHand = "pair";
            }
            if (winningCard.Count() == 0)
            {
                winningCard = Scoring.handScoring.highCard(blackHand, whiteHand);
                winningHand = "highCard";
            }
            foreach (var item in winningCard)
            {
                Console.WriteLine();
                Console.WriteLine();
                Console.Write(item.Key);
                Console.Write(" won with a ");
                Console.Write(item.Value.Symbol);
                Console.Write(item.Value.Suit);
                Console.Write(" in ");
                Console.Write(winningHand);
                Console.WriteLine();
            }
        }

    }

    public class Cards : Deck
    {
        public int[] value = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };
        public string[] symbol = { "2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A" };
        public string[] suit = { "C", "D", "H", "S" };

        public Cards() { }
        public Cards(int value, string symbol, string suit)
        {
            Value = value;
            Symbol = symbol;
            Suit = suit;
        }
        public int Value { get; set; }
        public string Symbol { get; set; }
        public string Suit { get; set; }

        public static List<Cards> organizeHandbyValue (List<Cards> unorderedHand)
        {
            List<Cards> SortedList = unorderedHand.OrderByDescending(o => o.Value).ToList();
            return SortedList;    
        }
    }
}




